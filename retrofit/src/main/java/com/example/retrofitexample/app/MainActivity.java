package com.example.retrofitexample.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.example.retrofitexample.app.adapter.QuoteListAdapter;
import com.example.retrofitexample.app.model.QuoteModel;
import com.example.retrofitexample.app.web.QuoteDownloader;
import com.example.retrofitexample.app.web.UmorilApi;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.android.MainThreadExecutor;
import retrofit.client.Response;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class MainActivity extends ActionBarActivity {

    private ListView mQuoteList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mQuoteList = (ListView) findViewById(R.id.quote_list);
//        getQuotes();
//        getQuotesWithCallback();
        getQuotesWithCallbackAndExecutors();
    }

    private void getQuotes() {
        QuoteDownloader downloader = new QuoteDownloader(new QuoteDownloader.OnQuoteDownload() {
            @Override
            public void OnQuoteDownloaded(List<QuoteModel> quotes) {
                mQuoteList.setAdapter(new QuoteListAdapter(MainActivity.this, R.layout.quote_row, quotes));
            }
        });
        downloader.execute(null, null, null);
    }

    private void getQuotesWithCallback() {
        RestAdapter apiAdapter = new RestAdapter.Builder().setEndpoint("http://www.umori.li").build();
        final UmorilApi api = apiAdapter.create(UmorilApi.class);
        api.getQuotes("bash", "bash.im", 10, new Callback<List<QuoteModel>>() {
            @Override
            public void success(List<QuoteModel> quotes, Response response) {
                mQuoteList.setAdapter(new QuoteListAdapter(MainActivity.this, R.layout.quote_row, quotes));
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Best app ever", "Shit happens :(", retrofitError);
            }
        });
    }

    private void getQuotesWithCallbackAndExecutors() {
        Executor httpExecutor = Executors.newFixedThreadPool(10);
        Executor callbackExecutor = new MainThreadExecutor();
        RestAdapter apiAdapter = new RestAdapter.Builder().setEndpoint("http://www.umori.li").setExecutors(httpExecutor, callbackExecutor).build();
        final UmorilApi api = apiAdapter.create(UmorilApi.class);
        api.getQuotes("bash", "bash.im", 10, new Callback<List<QuoteModel>>() {
            @Override
            public void success(List<QuoteModel> quotes, Response response) {
                mQuoteList.setAdapter(new QuoteListAdapter(MainActivity.this, R.layout.quote_row, quotes));
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                Log.e("Best app ever", "Shit happens :(", retrofitError);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
