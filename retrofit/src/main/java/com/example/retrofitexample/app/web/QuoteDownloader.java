package com.example.retrofitexample.app.web;

import android.os.AsyncTask;
import com.example.retrofitexample.app.model.QuoteModel;
import retrofit.RestAdapter;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Created by Anton on 4/19/2014.
 */
public class QuoteDownloader extends AsyncTask<Void, Void, List<QuoteModel>> {

    private final OnQuoteDownload mCallback;

    public QuoteDownloader(OnQuoteDownload callback){
        mCallback = callback;
    }

    @Override
    protected List<QuoteModel> doInBackground(Void... params) {
        RestAdapter apiAdapter = new RestAdapter.Builder().setEndpoint("http://www.umori.li").build();
        final UmorilApi api = apiAdapter.create(UmorilApi.class);
        List<QuoteModel> quotes = api.getQuotes("bash", "bash.im", 10);
        return quotes;
    }

    @Override
    protected void onPostExecute(List<QuoteModel> result) {
        mCallback.OnQuoteDownloaded(result);
    }

    public static interface OnQuoteDownload{
        void OnQuoteDownloaded(List<QuoteModel> quotes);
    }
}
