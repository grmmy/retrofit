package com.example.retrofitexample.app.adapter;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.retrofitexample.app.R;
import com.example.retrofitexample.app.model.QuoteModel;

import java.util.List;

/**
 * Created by Anton on 4/19/2014.
 */
public class QuoteListAdapter extends ArrayAdapter<QuoteModel> {

    private final Context mContext;
    private final List<QuoteModel> mQuotes;

    public QuoteListAdapter(Context context, int resource, List<QuoteModel> objects) {
        super(context, resource, objects);
        mContext = context;
        mQuotes = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        QuoteRowHolder holder;
        if(row == null){
            row = ((Activity) mContext).getLayoutInflater().inflate(R.layout.quote_row, parent, false);
            holder = new QuoteRowHolder();
            holder.desc = (TextView) row.findViewById(R.id.desc);
            holder.quote = (TextView) row.findViewById(R.id.quoteText);
            holder.name = (TextView) row.findViewById(R.id.name);
            row.setTag(holder);
        } else {
            holder = (QuoteRowHolder) row.getTag();
        }
        holder.desc.setText(mQuotes.get(position).desc);
        holder.name.setText(mQuotes.get(position).name);
        holder.quote.setText(Html.fromHtml(mQuotes.get(position).elementPureHtml));
        return row;
    }

    private class QuoteRowHolder{
        public TextView quote;
        public TextView desc;
        public TextView name;
    }
}
