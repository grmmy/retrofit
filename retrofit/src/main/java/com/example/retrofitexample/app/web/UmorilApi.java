package com.example.retrofitexample.app.web;

import com.example.retrofitexample.app.model.QuoteModel;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.List;

/**
 * GEO IP API Retrofit interface
 * Created by Anton on 4/19/2014.
 */
public interface UmorilApi {
    @GET("/api/get")
    public List<QuoteModel> getQuotes(@Query("name") String name, @Query("site") String site, @Query("num")int count);

    @GET("/api/get")
    public void getQuotes(@Query("name") String name, @Query("site") String site, @Query("num")int count, Callback<List<QuoteModel>> callback);
}
