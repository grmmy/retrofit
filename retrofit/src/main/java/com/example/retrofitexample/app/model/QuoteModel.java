package com.example.retrofitexample.app.model;

/**
 * Model which describes site quote
 *
 * Created by Anton on 4/19/2014.
 */
public class QuoteModel {
    public String site;
    public String name;
    public String desc;
    public String link;
    public String elementPureHtml;
}
